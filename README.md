# Install and use git ftp on windows

This is a guide to install git-ftp on windows. If you are using Linux/Unix based, see [link]https://github.com/resmo/git-ftp/blob/develop/INSTALL.md[/link]

## 1. Download msysGit

	http://msysgit.github.io/

Be patient it take so long

## 2. Execute c:\msysgit\msys.bat

	cd ~
	git clone https://github.com/git-ftp/git-ftp git-ftp.git
	cd git-ftp.git && chmod +x git-ftp
	cp ~/git-ftp.git/git-ftp /bin/git-ftp


## 3. We need an existing repository

Just to remember...

Start repository

	cd /myrepository
	git init
	git remote add origin https://user@bitbucket.org/user/repository.git
	git add .
	git commit -m 'initial commit'
	git push -u origin master

Or use existing one

	git remote add origin https://user@bitbucket.org/user/repository.git
	git push -u origin --all # pushes up the repo and its refs for the first time
	git push -u origin --tags # pushes up any tags

## 4. Configure git ftp

	git config git-ftp.user user
	git config git-ftp.url ftp.domain.com/http_path
	git config git-ftp.password password

## 5. And upload files	

Init ftp and upload first time
	
	git ftp init

And next time update code
	
	git ftp push